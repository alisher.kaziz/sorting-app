# Sorting App

This is my simple maven based project for my course on EPAM Training. The Goal of this projec is to practice my Maven building skills and test my understanding of JUnit.

## Project Details

### GAV Settings
- GroupId: `com.epam`
- ArtifactId: `sorting-app`
- Version: `1.0.0`

### Project Structure
- Java Source and Target Version: `8`
- Maven Model Version: `4.0.0`
- Maven Properties are used for configuration of junit4, collections4, jar-plugin, source encoding.

### Dependencies
- [JUnit v4.12](https://junit.org/junit4/index.html) library is included.


### Runnable JAR
- The project is configured to build a runnable JAR.
- Execute the JAR by running: `java -jar target/sorting-app-1.0.x.jar`

### Javadoc
- Javadoc for the public API is available in the `target/site/apidocs` directory.

## How to Use

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/alisher.kaziz/sorting-app.git
   ```
2. Navigate to the project directory:

   ```bash
   cd sorting-app
   ```
3. Build the project:

   ```bash
   mvn clean package
   ```
4. Run the application:

   ```bash
   java -jar target/sorting-app-1.0.x.jar
   ```

5. To run the unit tests:

   ```bash
   mvn test
   ```
