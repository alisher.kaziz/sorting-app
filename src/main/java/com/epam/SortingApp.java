package com.epam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Sorting App main class to operate with User
 * @author Alisher Kaziz
 */
public class SortingApp {

    /**
     * Main function which Runs the Program
     * @param args - arguments for sorting
     * @throws IOException - throws Exception if Input or Output is not valid
     */
    public static void main(String[] args) throws IOException {
        runProgram(args);
    }

    /**
     * Function which Runs the Program itself
     * @param args - arguments for sorting
     * @throws IOException - throws Exception if Input or Output is not valid
     */
    private static void runProgram(String[] args) throws IOException {
        List<Integer> ints = parseNumbers(args);

        Sorting.sort(ints);

        printNumbers(ints);
    }

    /**
     * Function to parse input to adjust to Array of int values
     * @param numbers - String of Numbers
     * @return - List of int values
     * @throws IOException - throws Exception if Input or Output is not valid
     */
    private static List<Integer> parseNumbers(String[] numbers) throws IOException {
        if (numbers.length > 10) {
            throw new IOException("No more than 10 numbers");
        }

        List<Integer> ints = new ArrayList<>((numbers.length+1)/2);

        try {
            for (String number : numbers) {
                ints.add(Integer.parseInt(number));
            }
        } catch (NumberFormatException e) {
            throw new IOException("Only numbers are allowed!");
        }
        return ints;
    }

    /**
     * Print values of sorted array
     * @param ints - Array of sorted values
     */
    private static void printNumbers(List<Integer> ints) {
        for (int integer : ints) {
            System.out.print(integer + " ");
        }
    }
}
