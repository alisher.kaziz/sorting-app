package com.epam;

import java.util.List;
import java.util.Collections;

/**
 * Sort input List
 * @author Alisher Kaziz
 */
public class Sorting {
    private Sorting() {}

    /**
     * This is the method that sort the given list of int values.
     *
     * @param list - List of integers
     */
    public static void sort(List<Integer> list) {
        Collections.sort(list);
    }

}
