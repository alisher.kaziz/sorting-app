package com.epam;
import java.io.IOException;
import java.util.Scanner;

/**
 * Main Class of my project just to execute
 * @author Alisher Kaziz
 */
public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter up to 10 integers:");
        String[] strArray = null;
        String input = scanner.nextLine();
        strArray = input.split(" ");
        SortingApp.main(strArray);
    }
}
