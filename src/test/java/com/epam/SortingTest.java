package com.epam;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingTest {
    private final List<Integer> toBeSorted;
    private final List<Integer> expectedOrder;

    public SortingTest(Integer[] toBeSorted, Integer[] expectedOrder) {
        this.toBeSorted = new ArrayList<>();
        CollectionUtils.addAll(this.toBeSorted, toBeSorted);
        this.expectedOrder = new ArrayList<>();
        CollectionUtils.addAll(this.expectedOrder, expectedOrder);
    }

    @Parameterized.Parameters(name = "test with {index}")
    public static Collection<Object[]> data() {
        return Arrays.asList(
                new Object[][]
                        {
                                {new Integer[]{3, 1, 4, 6, 7, 9}, new Integer[]{1, 3, 4, 6, 7, 9}},
                                {new Integer[]{3, 1, 4, 6, 8, 9, 2, 5}, new Integer[]{1, 2, 3, 4, 5, 6, 8, 9}},
                                {new Integer[]{9, 8, 7, 6}, new Integer[]{6, 7, 8, 9}},
                                {new Integer[]{3, 1}, new Integer[]{1, 3}},
                                {new Integer[]{9}, new Integer[]{9}},
                                {new Integer[]{}, new Integer[]{}},
                                {new Integer[]{3, 1, 4, 6, 8, 9, 2, 5, 11, 14, 12, 15}, new Integer[]{1, 2, 3, 4, 5, 6, 8, 9, 11, 12, 14, 15}},
                                {new Integer[]{1, 1, 1, 1}, new Integer[]{1, 1, 1, 1}},
                                {new Integer[]{9, 8, 7, 6, 5, 4}, new Integer[]{4, 5, 6, 7, 8, 9}},
                                {new Integer[]{1, 3, 4, 5, 7, 9}, new Integer[]{1, 3, 4, 5, 7, 9}},
                        });
    }

    @Test
    public void sortsArrayCorrectly() {
        Sorting.sort(toBeSorted);
        assertEquals(expectedOrder, toBeSorted);
    }
}

