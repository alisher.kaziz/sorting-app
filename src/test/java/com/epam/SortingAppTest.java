package com.epam;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class SortingAppTest {
    @Test(expected = IOException.class)
    public void testOverBound() throws IOException {
        String[] input = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
        SortingApp.main(input);
    }

    @Test(expected = IOException.class)
    public void testNonNumeric() throws IOException {
        String[] input = new String[]{"test"};
        SortingApp.main(input);
    }

    @Test
    public void testSortedArraysCase() {
        final ByteArrayOutputStream sink = new ByteArrayOutputStream();
        PrintStream controlledOut = new PrintStream(sink);
        PrintStream defaultOut = System.out;
        System.setOut(controlledOut);

        try {
            SortingApp.main(new String[]{"2", "1", "4", "3", "5"});
            controlledOut.flush();
            final String actual = sink.toString().trim();
            assertEquals("1 2 3 4 5", actual);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            System.setOut(defaultOut);
        }
    }
}
